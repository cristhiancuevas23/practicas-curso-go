package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	rutinas := 100
	contador := 0
	wg.Add(rutinas)
	var m sync.Mutex

	for i := 0; i < rutinas; i++ {

		go func() {
			m.Lock()
			v := contador
			v++
			contador = v
			fmt.Println("Rutina numero", contador)
			m.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()

}
