package main

import "fmt"

func main() {

	p := make(chan int)
	i := make(chan int)
	s := make(chan struct{})

	go enviar(p, i, s)
	recibir(p, i, s)

	fmt.Println("Finalizando programa")
}

// Envia a todos los canales
func enviar(p, i chan<- int, s chan<- struct{}) {
	for c := 0; c < 100; c++ {
		if c%2 == 0 {
			p <- c
		} else {
			i <- c
		}
	}
	s <- struct{}{}
}

// Envia a todos los canales
func recibir(p, i <-chan int, s <-chan struct{}) {
	for {
		select {
		case v := <-p:
			fmt.Println("Es par", v)
		case v := <-i:
			fmt.Println("Es impar", v)
		case v := <-s:
			fmt.Println("Saliendo", v)
			return
		}

	}
}
