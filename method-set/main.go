package main

import (
	"fmt"
	"practicas-curso-go/method-set/hola"
)

type persona struct {
	Nombre   string
	Apellido string
	Edad     int
}

func (p *persona) hablar() {
	fmt.Println("Hola mi nomnbre es", p.Nombre, p.Apellido)
}

type humano interface {
	hablar()
}

func diAlgo(h humano) {
	h.hablar()
}

func main() {

	p := persona{
		"Cristhian",
		"Cuevas",
		27,
	}
	diAlgo(&p)
	fmt.Println(hola.Sumar(1, 2, 3))

}
