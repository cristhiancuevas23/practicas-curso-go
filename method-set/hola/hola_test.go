package hola

import "testing"

func TestSumar(t *testing.T) {

	type test struct {
		numeros   []int
		respuesta int
	}

	tests := []test{
		test{[]int{1, 3, 4}, 8},
		test{[]int{1, 4, 4}, 9},
		test{[]int{1, 3, 6}, 10},
	}

	for _, valor := range tests {
		v := Sumar(valor.numeros...)
		if v != valor.respuesta {
			t.Error("Expected", valor.respuesta, "Got", v)
		}
	}

}
